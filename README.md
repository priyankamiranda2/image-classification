

Techniques Used:
1)	Scale-invariant feature transform (SIFT)
2)	Bag of Visual words
3)	K Means Clustering
4)	SVM Classification

Usage
To run the main program run `python main.py`

Dependencies
Used with OpenCV 3 and Python 3.5. Python libraries required are scipy, numpy and matplotlib.


