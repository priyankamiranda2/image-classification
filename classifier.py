import cv2
import numpy as np
import time
import os 

from sklearn.svm import LinearSVC 
from sklearn.svm import OneClassSVM
from sklearn.externals import joblib
from sklearn.cluster import MiniBatchKMeans
# Local dependencies

import constants
import descriptors
import filenames
import utils


from sklearn.model_selection import cross_validate
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB

from keras.models import load_model
import pickle
class Classifier:
    """
    Class for making training and testing in image classification.
    """
    def __init__(self, dataset, log,class_names_dict):
        """
        Initialize the classifier object.
        Args:
            dataset (Dataset): The object that stores the information about the dataset.
            log (Log): The object that stores the information about the times and the results of the process.

        Returns:
            void
        """
        self.dataset = dataset
        self.log = log
        self.class_names_dict=class_names_dict
#------------------------------------------------------------------------------------------------------
    def train(self, svm_kernel, k, des_name, compare_models,isDict,des_option=constants.ORB_FEAT_OPTION, is_interactive=True):
        """
        Gets the descriptors for the training set and then calculates the SVM for them.

        Args:
            svm_kernel (constant): The kernel of the SVM that will be created.
            k: number of classes 
            des_name: description name
            compare_models: use multiple models or not (boolean)
            isDict: if dict is present, do not have to train from individual images. Train from dict.
            des_option (integer): The option of the feature that is going to be used as local descriptor.
            is_interactive (boolean): If it is the user can choose to load files or generate.

        Returns:
            cv2.SVM: The Support Vector Machine obtained in the training phase.
        """
        isTrain= True
        des_name = constants.ORB_FEAT_NAME if des_option == constants.ORB_FEAT_OPTION else constants.SIFT_FEAT_NAME
        print("Getting global descriptors for the training set.")
        start = time.time()
        des, x, y, cluster_model = self.get_data_and_labels(self.dataset.get_train_set(),None, k, des_name, np.array([]),isTrain,isDict,des_option)
        end = time.time()
        if not isDict:
            utils.save(des, y,self.class_names_dict)
        svm_filename = filenames.svm(k, des_name, svm_kernel)
        print("Traning models with training set...")
        params=dict()
        if not compare_models:
            #---------Multi class classifier-----------------
            models = LinearSVC()
            models.fit(x,y)
            #---------Single class classifier-----------------
            # svm = OneClassSVM()
            # svm.fit(x)
            #----------all models---------------------------
            scoring = ['accuracy', 'precision_macro', 'recall_macro' , 'f1_weighted', 'roc_auc']
            scores = cross_validate(models, x, y, scoring=scoring, cv=2)
            sorted(scores.keys())
            fit_time = scores['fit_time'].mean()
            score_time = scores['score_time'].mean()
            accuracy = scores['test_accuracy'].mean()
            precision = scores['test_precision_macro'].mean()
            recall = scores['test_recall_macro'].mean()
            f1 = scores['test_f1_weighted'].mean()
            roc = scores['test_roc_auc'].mean()  
            params[str(models).split('(')[0]]={'fit_time':fit_time,'score_time':score_time,
                'accuracy':accuracy,'precision':precision,
                'recall':recall,'f1':f1,'roc':roc}
            classifier_filename=filenames.classifier_name(str(models).split('(')[0])
            pickle.dump(models, open(classifier_filename, 'wb'))
            return models , cluster_model , params
        else:
            # models = [LogisticRegression(),DecisionTreeClassifier(),SVC(kernel='linear'),LinearSVC()
            # LinearDiscriminantAnalysis(),QuadraticDiscriminantAnalysis(),RandomForestClassifier(),
            # KNeighborsClassifier(),GaussianNB()]
            models = [LogisticRegression(),DecisionTreeClassifier(),SVC(kernel='linear'),LinearSVC(),
            LinearDiscriminantAnalysis()]
            params=dict()
            for model in models:
                print('Fitting for model '+str(model).split('(')[0])
                model.fit(x,y)
                scoring = ['accuracy', 'precision_macro', 'recall_macro' , 'f1_weighted', 'roc_auc']
                scores = cross_validate(model, x, y, scoring=scoring, cv=2)
                sorted(scores.keys())
                fit_time = scores['fit_time'].mean()
                score_time = scores['score_time'].mean()
                accuracy = scores['test_accuracy'].mean()
                precision = scores['test_precision_macro'].mean()
                recall = scores['test_recall_macro'].mean()
                f1 = scores['test_f1_weighted'].mean()
                roc = scores['test_roc_auc'].mean()    
                params[str(model).split('(')[0]]={'fit_time':fit_time,'score_time':score_time,
                                'accuracy':accuracy,'precision':precision,
                                'recall':recall,'f1':f1,'roc':roc} 
                classifier_filename=filenames.classifier_name(str(model).split('(')[0])
                pickle.dump(model, open(classifier_filename, 'wb'))
            return models , cluster_model, params
#------------------------------------------------------------------------------------------------------
    def test(self, models, cluster_model, k,compare_models,des_option = constants.ORB_FEAT_OPTION, is_interactive=True):
        """
        Gets the descriptors for the testing set and use the svm given as a parameter to predict all the elements

        Args:

            k: number of classes 
            compare_models: use multiple models or not (boolean)
            isDict: if dict is present, do not have to train from individual images. Train from dict.
            codebook (NumPy matrix): Each row is a center of a codebook of Bag of Words approach.

            svm (cv2.SVM): The Support Vector Machine obtained in the training phase.
            des_option (integer): The option of the feature that is going to be used as local descriptor.
            is_interactive (boolean): If it is the user can choose to load files or generate.

        Returns:
            NumPy float array: The result of the predictions made.
            NumPy float array: The real labels for the testing set.
        """
        isTrain = False
        isDict= False
        des_name = constants.ORB_FEAT_NAME if des_option == constants.ORB_FEAT_OPTION else constants.SIFT_FEAT_NAME
        print("Getting global descriptors for the testing set...")
        start = time.time()
        des,x, y, cluster_model= self.get_data_and_labels(self.dataset.get_test_set(), cluster_model, k, des_name,np.array([]),isTrain,isDict,des_option)
        end = time.time()
        accu_dict=dict()
        if not compare_models:# Only svm
            start = time.time()   
            result = models.predict(x)
            new_result=list()
            for x in np.nditer(result):
                new_result.append([x])
            new_result=np.array(new_result)
            end = time.time()
            self.log.predict_time(end - start)
            #---------For multiple class classifier only-----------
            mask = new_result == y
            #---------For single class classifier only-----------
            # mask = new_result == 1
            correct = np.count_nonzero(mask)
            accuracy = (correct * 100.0 / result.size)
            accu_dict[str(models).split('(')[0]]=accuracy
            self.log.accuracy(accu_dict)
        else:
            result=dict()
            predict_time_dict=dict()
            for model in models:
                start = time.time()   
                result[model] = model.predict(x)
                new_result=list()
                for x_elem in np.nditer(result[model]):
                    new_result.append([x_elem])
                new_result=np.array(new_result)
                end = time.time()
                predict_time_dict[str(model).split('(')[0]]=end - start
                #---------For multiple class classifier only-----------
                mask = new_result == y
                #---------For single class classifier only-----------
                # mask = new_result == 1
                correct = np.count_nonzero(mask)
                accuracy = (correct * 100.0 / result[model].size)
                print("Accuracy: ")
                print(accuracy)
                accu_dict[str(model).split('(')[0]]=accuracy
            self.log.predict_time(predict_time_dict)
            self.log.accuracy(accu_dict)
        return result, y, self.log
#------------------------------------------------------------------------------------------------------
    def get_data_and_labels(self, img_set, cluster_model, k, des_name, codebook,isTrain, isDict,des_option = constants.ORB_FEAT_OPTION):
        """
        Calculates all the local descriptors for an image set and then uses a codebook to calculate the VLAD global
        descriptor for each image and stores the label with the class of the image.
        Args:
            img_set (string array): The list of image paths for the set.
            codebook (numpy float matrix): Each row is a center and each column is a dimension of the centers.
            des_option (integer): The option of the feature that is going to be used as local descriptor.

        Returns:
            NumPy float matrix: Each row is the global descriptor of an image and each column is a dimension.
            NumPy float array: Each element is the number of the class for the corresponding image.
        """
        y = []
        x = None
        des = []
        multiple_descriptors=False
        if isDict == 1:#Dictionary is there. Traning from dictionary.
            #Read descriptors from file
            files = next(os.walk(constants.FILES_DIR_NAME))[2]
            print(files)
            for dict_files in files:
                try:
                    class_name=str(dict_files).split("Dictionary_train_class_")[1].split('_')[0]
                    dimens=str(dict_files).split("_dimens_")[1].split('.')[0]
                    class_number=list(self.class_names_dict.keys())[list(self.class_names_dict.values()).index(class_name)]
                    file_path=constants.FILES_DIR_NAME+"/"+dict_files
                except:
                    continue 
                des,y=utils.load(file_path,class_number,des,y)
        else:#Reading images.Runs for both traning and testing option
            if not multiple_descriptors ==1:
                for class_number in range(len(img_set)):
                    img_paths = img_set[class_number]
                    n=0
                    print(" ")
                    for i in range(len(img_paths)):
                        print('Class: '+str(class_number)+' Reading Image '+str(n+1)+'/'+str(len(img_paths)), end='\r')
                        img = cv2.imread(img_paths[i])     
                        des,y = descriptors.surf(img,des,y,class_number)
                        n+=1
            else:
                internal_descriptors=dict()
                cluster_models=dict()
                for class_number in range(len(img_set)):
                    img_paths = img_set[class_number]
                    n=0
                    print(" ")
                    for i in range(len(img_paths)):
                        print('Class: '+str(class_number)+' Reading Image '+str(n+1)+'/'+str(len(img_paths)), end='\r')
                        img = cv2.imread(img_paths[i])     
                        internal_descriptors,y = descriptors.get_descriptors(img,internal_descriptors,y,class_number)
                        n+=1
                    print(internal_descriptors)
                    input()
                if isTrain == 1:#If traning
                    print("\n Clustering datapoints..")
                    for key in internal_descriptors.keys():
                        X, cluster_model = descriptors.cluster_features(internal_descriptors[key],cluster_model=MiniBatchKMeans(n_clusters=1000))
                        cluster_models[key]=cluster_model
                        print(type(X))
                        input()
                else:#If testing
                    X = descriptors.img_to_vect(des,cluster_model)
                y = np.float32(y)[:,np.newaxis]
                x = np.matrix(X)
                return des, x, y, cluster_model
        if isTrain == 1:#If traning
            print("\n Clustering datapoints..")
            X, cluster_model = descriptors.cluster_features(des,cluster_model=MiniBatchKMeans(n_clusters=10000))
            cluster_model_filename=filenames.cluster_model_name(str(cluster_model).split('(')[0])
            pickle.dump(cluster_model, open(cluster_model_filename, 'wb'))# creates a pickle file
        else:#If testing
            X = descriptors.img_to_vect(des,cluster_model)
        # print('X',X.shape)
        y = np.float32(y)[:,np.newaxis]
        x = np.matrix(X)
        return des, x, y, cluster_model