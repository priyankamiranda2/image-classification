NUMPY_DELIMITER = ","
PICKLE_PROTOCOL = 2
DATASET_PATH = "/home/office/Documents/Priyanka/image_classification/imageDataset"
ORB_FEAT_OPTION = 1
SIFT_FEAT_OPTION = 2
ORB_FEAT_NAME = "ORB"
SIFT_FEAT_NAME = "SIFT"
GENERATE_OPTION = 1
DESCRIPTORS_SELECTED={"harris_corner":0,"shi_tomasi":0,"sift":0,"surf":1,"orb":1,"brisk":1,"brief":1,"fast":1}
DATASET_PARTITION="75:25"
INTERNAL_FILE='100'
FILES_DIR_NAME = DATASET_PATH+"/files/final_results/"+INTERNAL_FILE+"/"+DATASET_PARTITION
TRAIN_TXT_FILE = "train.txt"
TEST_TXT_FILE = "test.txt"
STEP_PERCENTAGE = 20
#------------------------------------------------------------------------------------------------------