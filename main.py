import cv2
import numpy as np
import time
import os
import sys
# Local dependencies
from classifier import Classifier
from dataset import Dataset
import descriptors
import constants
import utils
import filenames
from log import Log
import glob
import constants 
import json
from keras.models import load_model

def main(is_interactive=True, k=64):
    experiment_start = time.time()
    # Check for the dataset of images
    if not os.path.exists(constants.DATASET_PATH):
        print("Dataset not found, please copy one.")
        return
    if not os.path.exists(constants.FILES_DIR_NAME):
        os.makedirs(constants.FILES_DIR_NAME)


    #Getting the dataset path and generating sets
    dataset = Dataset(constants.DATASET_PATH)
    classes_count, class_names_dict=dataset.generate_sets()

    # Create directory to store results generated files
    if not os.path.exists(constants.FILES_DIR_NAME):
        os.makedirs(constants.FILES_DIR_NAME)

    # Setting all parameters
    if is_interactive:
        des_option = 2# option for descriptor
        k = classes_count#clustering parameter
        svm_option = 2# option for kernel 
    kernel = cv2.ml.SVM_LINEAR if svm_option == 1 else cv2.ml.SVM_RBF
    des_name = constants.ORB_FEAT_NAME if des_option == constants.ORB_FEAT_OPTION else constants.SIFT_FEAT_NAME
    log = Log(k, des_name, kernel)

    codebook_filename = filenames.codebook(k, des_name)
    print('Codebook filename:')
    print(codebook_filename)
    compare_models=True
    isDict=False
    isTrained_model=False
    # Train and test the dataset
    classifier = Classifier(dataset, log,class_names_dict)
    if not isTrained_model:
        start = time.time()   
        print("Training based on clustering variable descriptors for each class")
        train_models, cluster_model ,params= classifier.train(kernel, k, des_name,compare_models,isDict, des_option=des_option, is_interactive=is_interactive)
        end = time.time()
        log.train_des_time(end - start)
        log.params(params)
    else:
        os.chdir(constants.FILES_DIR_NAME)
        model_extension = 'h5'
        cluster_model_extension = 'pickle'
        model_locs = glob.glob('*.{}'.format(model_extension))
        cluster_model_loc = glob.glob('*.{}'.format(cluster_model_extension))
        print(result)
        input()

        for model_loc in model_locs:
            model.append(load_model(constants.FILES_DIR_NAME+'/'+model_loc))
            class_name=str(files).split(".h5")[1]
            print(class_name)
            input()
                # dimens=str(dict_files).split("_dimens_")[1].split('.')[0]
                # class_number=list(self.class_names_dict.keys())[list(self.class_names_dict.values()).index(class_name)]
                # file_path=constants.FILES_DIR_NAME+"/"+dict_files
        # print(classifier_models)
        # for x in classifier_models:
        #     print(x.split('.'))
        #     input()

    print("Training done. Now beginning with testing on the model")
    start = time.time()
    result, labels, log = classifier.test(train_models,cluster_model, k,compare_models,des_option=des_option, is_interactive=is_interactive)
    end = time.time()
    log.codebook_time(end - start)
    # Store the results from the test
    classes = dataset.get_classes()
    log.classes(classes)
    result_filename = filenames.result(k, des_name, kernel)
    confusion_matrix_dict=dict()
    if not compare_models:
        # --------------Multi class confusion matrix-----------------
        # Create a confusion matrix
        confusion_matrix = np.zeros((len(classes), len(classes)), dtype=np.uint32)
        for i in range(len(result)):
            predicted_id = int(result[i])
            real_id = int(labels[i])
            confusion_matrix[real_id][predicted_id] += 1

        print("Confusion Matrix =\n{0}".format(confusion_matrix))
        log.confusion_matrix(confusion_matrix)
    else:
        for key in result:

            confusion_matrix = np.zeros((len(classes), len(classes)), dtype=np.uint32)
            for i in range(len(result[key])):
                predicted_id = int(result[key][i])
                real_id = int(labels[i])
                confusion_matrix[real_id][predicted_id] += 1

            print("Confusion Matrix =\n{0}".format(confusion_matrix))
            confusion_matrix_dict[str(key).split('(')[0]]=str(confusion_matrix)
    log.confusion_matrix(confusion_matrix_dict)
    log.save()
    print("Log saved on {0}.".format(filenames.log(k, des_name, kernel)))
        
    experiment_end = time.time()
    elapsed_time = utils.humanize_time(experiment_end - experiment_start)
    print("Total time during the experiment was {0}".format(elapsed_time))
    # else:
    #     # Show a plot of the confusion matrix on interactive mode
    #     utils.show_conf_mat(confusion_matrix)
#------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    # constants.DATASET_PATH = sys.argv[1]
    main()