import os 
import time
import multiprocessing 
import sys
import math 
import psutil
from multiprocessing import Pool, cpu_count 

def main(orig_loc,files):
	starttime = time.time()
	processes = []
	for elem in files:
		p = multiprocessing.Process(target=commandx, args=(orig_loc,elem))
		p2=psutil.Process(os.getpid())
		p2.nice(19)
		processes.append(p)
		p.start()

	for process in processes:
		process.join()
		print('Time taken is {} seconds'.format(time.time() - starttime))
	
def commandx(orig_loc,elem):
	pythonCommand = 'python3.5 main.py '+orig_loc+elem
	os.system(pythonCommand)


if __name__=='__main__':

	orig_loc="/home/sumedh/Documents/Priyanka/Flipshope/implementations/image_classification/ToShivam/"
	files=["1","3","5","7","8"]
	num=num_per_iter=1
	start=0

	for reps in range(math.ceil(len(files)/num_per_iter)):
		print("Classes considered for curent iteration: "+str(files[start:num]))
		start=start+num_per_iter
		num=num+num_per_iter		
		main(orig_loc,files[start:num])

