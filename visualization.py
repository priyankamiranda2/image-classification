import numpy 
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import seaborn as sns
import matplotlib.pyplot as plt  
import constants 
import sys  
sys.path.append(constants.FILES_DIR_NAME)  
import log_SIFT 

def visualize_results():
	training_dict=log_SIFT.Training_results
	x_axis=list(training_dict[list(training_dict.keys())[0]].keys())
	y_axis=list()
	values=list()
	n=0
	for keys in training_dict:
		y_axis.append(str(keys).split('(')[0][:10])
		int_vals=list()
		for elem in x_axis:
			int_vals.append(training_dict[keys][elem])	
		values.append(int_vals)

	plt.rcParams.update({'font.size': 5})
	ax= plt.subplot()
	sns.set(style="white")
	sns.heatmap(values, annot=True, ax = ax,annot_kws={"size":10}); 
	sns.set(font_scale=0.01)
	ax.set_xlabel('Parameters');
	ax.set_ylabel('Classifier'); 
	ax.set_title('Confusion Matrix'); 
	ax.xaxis.set_ticklabels(x_axis); 
	ax.yaxis.set_ticklabels(y_axis);
	plt.savefig(constants.FILES_DIR_NAME+'/training_params_matrix.png')
	print("Confusion matrix saved in : "+str(constants.FILES_DIR_NAME)+'/training_params_matrix.png')
	print("-------------------------")
	testing_dict=log_SIFT.Testing_prediction_time
	print("Testing time: ")
	print(testing_dict)
	print("-------------------------")
	Accu_dict=log_SIFT.Accu_dict
	print("Accuracy: ")
	print(Accu_dict)
	conf_matrix=log_SIFT.Confusion_Matrix_dict
	print("-------------------------")
	print("Confusion Matrix: ")
	for elem in conf_matrix:
		print(elem)
		print(conf_matrix[elem])
	print("-------------------------")
#------------------------------------------------------------------------------------------------------
if __name__=='__main__':
	visualize_results()
